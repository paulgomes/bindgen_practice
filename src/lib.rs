include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[cfg(test)]
mod tests {
    // TODO: figure out how to get bindgen to put these in namespace
    use memb;
    use memb_init;

    #[test]
    fn init_alloc_and_free() {
        // TODO: invoke memb_init, memb_alloc and memb_free
        // Will need to figure out how to link with contiki_memb library
        unsafe {

            // This is the C code that MEMB expands to for cTinyString.
            // I put these in wrapper.h. 
            /*
            static char allcTinyStrings_memb_count[1]; 
            static struct cTinyString allcTinyStrings_memb_mem[1]; 
            static struct memb allcTinyStrings = {sizeof(struct cTinyString), 1, 
                allcTinyStrings_memb_count, (void *)allcTinyStrings_memb_mem};
            */

            // TODO: figure out how to do this next
            //let mut my_memb_struct = memb { size:32, num:3, count:3, mem:3 };
            //memb_init(&mut my_memb_struct as *mut _);

            assert_eq!(1,1);
        }
    }
}
